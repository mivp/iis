import secrets
import hashlib
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--password')
parser.add_argument('-s', '--salt')
args = parser.parse_args()

password = ('admin' if not args.password else args.password).encode()

iters = 10000

salt = b"\x00" + \
    secrets.token_bytes(16) + b"\x00" if not args.salt else args.salt.enocde()

hash = hashlib.pbkdf2_hmac('sha1', password, salt, iters, dklen=64)
print('pbkdf2${}${}${}'.format(iters, hash.hex(), salt.hex()))
