## 1. Aufgabe: Konzeptioneller Datenbankentwurf

Sie arbeiten in einer Produktentwicklungsabteilung und möchten die Produkte Ihres Unternehmens in einer Datenbank speichern. Die IT Abteilung Ihres Unternehmens bestätigt, dass es prinzipiell kein Problem sei, eine Datenbank zu erstellen, sie wisse jedoch nicht über die konkreten Anforderungen und Beziehungen der Produkte Bescheid. Um diese Spezifikationen an die IT Abteilung zu übermitteln, wurden Sie beauftragt, ein Entity Relationship (ER) Diagramm zu erstellen, in welchem sowohl die Produktdaten, als auch deren inner- und außerbetrieblichen Beziehungen dargestellt werden. Dabei sollen die Grunddaten der Produkte bereits in der Struktur übermittelt werden, wie diese später in der Softwarelösung (z.B. SAP) verwendet werden sollen.

### Anforderungen

- Zu einem Produkt sollen wesentliche Grunddaten gespeichert werden. Dazu zählen
  Bezeichnung, EAN (European Article Number), Abmessungen, Gewicht und Werkstoff.
- Ein Produkt wird in einem Lager gelagert, wobei in einem Lager mehrere Produkte aufbewahrt 
  werden.
- Einem Produkt können mehrere Konstruktionsdokumente zugeordnet sein.
- Ein Produkt wird in genau einem Werk produziert. Innerhalb eines Werks werden mehrere 
  Produkte gefertigt.
- Ein Produkt hat genau einen verantwortlichen Produktmanager.
- Eine Produktfamilie hat eine Bezeichnung, sowie genau einen
  verantwortlichen Produktmanager für die Produktfamilie.
- Ein Produktmanager kann für mehrere Produkte und Produktfamilien
  verantwortlich sein.
- Ein Produkt gehört zu genau einer Produktfamilie. Einer Produktfamilie
  können mehrere Produkte zugehörig sein.
- Eigene Produkte können mit mehreren Produkten vom Mitbewerber konkurrieren
  und Produkte vom Mitbewerber können mit mehreren eigenen Produkten
  konkurrieren.
- Ein Mitbewerberprodukt hat eine EAN und eine Beschreibung.
- Ein Mitbewerber kann mehrere Konkurrenzprodukte herstellen. Ein
  Konkurrenzprodukt wird von einem Mitbewerber hergestellt.
- Ein Mitbewerber hat einen eindeutigen Namen.
- Ein Lager hat Ort, Kapazität und einen eindeutigen Namen.
- Ein Dokument hat eine Blattanzahl, Version, Dokumentenart und eine eindeutige Nummer
- Produktionswerke haben einen Namen und Ort.

### ER Diagramm

Transformieren Sie die textuelle Beschreibung und Anforderungen in ein
ER Diagramm. Legen Sie dabei Kardinalitäten für Beziehungstypen und
Primärschlüssel für Entitätstypen fest.

### Schema

Leiten Sie aus ihrem ER Diagramm ein Datenbankschema ab. Verlangt wird hier
noch kein SQL. Folgende Form ist für diese Aufgabe ausreichend:

> Tabellenbezeichnung (**Spalte1**, Spalte2, _Spalte3_, ... SpalteN)

Markieren sie **Primär-** und _Fremdschlüssel_.

#### Hinweis zur Erstellung des ER-Diagramms
Wir lassen Ihnen frei mit welchen Programm Sie das ER Diagramm zeichnen. Eine Möglichkeit ist Dia.

#### Hinweis zur Dokumentation
Speichern Sie Ihr ER Diagramm und Ihre Schemadefinition in einer PDF-Datei im Verzeichnis solutions/hw. Wie Sie die Hausübung abgeben können, ist hier beschrieben.


## 2. Aufgabe: Mitarbeiterverwaltung

Ihre Aufgabe ist es einfache SQL [Abfragen](#abfragen) zu erstellen.
Basis ist dieses ER Diagramm aus dem Vortrag zur Hausübung.

![Users](/public/images/org.png)

Aus dem ER Modell wurde folgenden Tabellendefinition erstellt. Dieselbe
Tabellendefinition finden sie in der Datei `assignments/hw/migration_up.sql`.

```sql
CREATE TABLE Mitarbeiter (
	SVNR INTEGER PRIMARY KEY,
	Vorname TEXT,
	Nachname TEXT,
	Stundensatz NUMERIC
);


-- 1:1 Abteilung wird von einem Mitarbeiter gemanagt
--     Mitarbeiter managt genau eine Abteilung
CREATE TABLE Abteilungen (
	Name TEXT PRIMARY KEY,
	Beschreibung TEXT,
	Manager INTEGER UNIQUE,
	FOREIGN KEY(Manager) REFERENCES Mitarbeiter(SVNR)
);


-- 1:N Mitarbeiter gehört zu genau einer Abteilung
--     einer Abteilung können mehrere Mitarbeiter zugehörig sein
CREATE TABLE arbeitet_in (
	Mitarbeiter INTEGER UNIQUE,
	Abteilung TEXT,
	FOREIGN KEY(Mitarbeiter) REFERENCES Mitarbeiter(SVNR),
	FOREIGN KEY(Abteilung) REFERENCES Abteilungen(Name)
);


-- M:N Mitarbeiter arbeitet an mehreren Projekten
--     in Projekten arbeiten mehrere Mitarbeiter
CREATE TABLE Projekte (
	Name TEXT PRIMARY KEY,
	Budget NUMBER
);

CREATE TABLE arbeitet_an (
	Mitarbeiter INTEGER,
	Projekt TEXT,
	Start DATE,
	Ende DATE,
	FOREIGN KEY(Mitarbeiter) REFERENCES Mitarbeiter(SVNR),
	FOREIGN KEY(Projekt) REFERENCES Projekte(Name)
);
```

Die Tabellen werden beim Start der Anwendung erstellt. D.h. für Ihre
Beispielanfragen können Sie davon ausgehen, dass die Tabellen existieren.

Um Testdaten einzufügen könne Sie im Administrationsbereich die Schaltfläche
`Testdaten einfügen` verwenden. Dafür ist es jedoch notwendig, dass Sie die
erste Abfrage bereits richtig erstellt haben.

#### Hinweis zur Erstellung der Abfragen

Sie finden die Dateien in denen Sie ihre SQL-Abfragen speichern im Ordner
`solutions/hw`. Falls sie Anwendung nicht verwenden, erstellen Sie die
entsprechenden Dateien (Textdateien mit der Endung ".sql") selbst. Anstatt
Werte direkt einzugeben, verwenden sie bitte Platzhalter, welche mit einem
`$` Zeichen beginnen.

Beispielsweise anstatt:

```sql
SELECT hashedpassword FROM Users WHERE login = 'admin';
```

schreiben sie:

```sql
SELECT hashedpassword FROM Users WHERE login = $login;
```

Der Platzhalter `$login` wird dann von der Anwendung mit dem eigentlichen Wert
ersetzt. Also mit `'admin'`, sollte sich der Administrator anmelden.
Dieses und weitere Beispiele finden Sie im Ordner `solutions/lecture1` mit
einer kurzen Erklärung [hier](/assignments/lecture1.md)

#### Hinweis zum Testen

Sie können die von Ihnen erstellten Abfragen mit dem Kommandozeilenbefehl
`npm run test hw` automatisch überprüfen. Dazu müssen Sie natürlich
die Anwendung installiert haben. Beschrieben in der [README](/).

### Abfragen

Ihre Abfragen sollen Funktionen zur Mitarbeiterverwaltung unterstützen. Die
Oberfläche ist nur Administratoren zugänglich. Melden Sie sich als `admin`
(Passwort: `admin`) an. Um sich anzumelden, finden sie in der rechten oberen
Ecke der Anwendung einen Link "Login". Wenn Sie sich angemeldet haben,
sehen sie ebenfalls in der rechten oberen Ecke der Anwendung einen Link
"Org".

Die Dateien in der Sie Ihre Abfragen schreiben können, finden sie im Ordner
`solutions/hw1`.

Auf Basis der gegebenen Tabellen erstellen Sie eine SQL-Abfrage...

1. ... mit der ein Mitarbeiter hinzugefügt werden kann.

   - Datei: `employee_add.sql`
   - Platzhalter: `$surname`, `$forename`, `$ssn`, `$rate`
   - Rückgabeformat: nicht relevant

2. ... mit der die Namen und Stundensätze aller Mitarbeiter abgefragt werden
   können. Beachten Sie, dass die Bezeichnungen im Rückgabeformat nicht mit den
   Spaltennamen in den vorhandenen Tabellen übereinstimmen. Sie können mit `AS`
   die Spalten für die Rückgabe umbenennen.

   - Datei: `employees_all.sql`
   - Platzhalter: nicht relevant
   - Rückgabeformat: `(ssn, surname, forename, rate)`

3. ... mit der ein Stundensatz eines Mitarbeiters um einen bestimmten Betrag
   erhöht werden kann.

   - Datei: `employee_increase_rate.sql`
   - Platzhalter: `$ssn`, `$rateIncrement`
   - Rückgabeformat: nicht relevant

4. ... mit der ein Mitarbeiter einer Abteilung zugewiesen werden kann.

   - Datei: `employee_add_to_department.sql`
   - Platzhalter: `$ssn`, `$departmentShort`
   - Rückgabeformat: nicht relevant

5. ... mit der die Sozialversicherungsnummeren aller Mitarbeiter in einer
   Abteilung abgefragt werden können.

   - Datei: `employees_in_department.sql`
   - Platzhalter: `$departmentShort`
   - Rückgabeformat: `(ssn)`

6. ... mit der der Manager einer Abteilung geändert werden kann.

   - Datei: `department_set_manager.sql`
   - Platzhalter: `$departmentShort`, `$ssn`
   - Rückgabeformat: nicht relevant

7. ... mit der eine Abteilung aus der Datenbank entfernt werden kann.

   - Datei: `department_remove.sql`
   - Platzhalter: `$departmentShort`
   - Rückgabeformat: nicht relevant

8. ... mit der der Mitarbeiter mit dem höchsten Stundensatz ausgegeben
   wird.

   - Datei: `employee_max_rate.sql`
   - Platzhalter: nicht relevant
   - Rückgabeformat: `(ssn, rate)`

9. ... mit der alle Mitarbeiter, die jemals in einem Projekt gearbeitet haben,
   abgefragt werden können. Achten Sie darauf, dass Mitarbeiter nicht doppelt im
   Ergebnis vorkommen.

   - Datei: `employees_in_project.sql`
   - Platzhalter: `$projectName`
   - Rückgabeformat: `(ssn)`

10. Erstellen Sie eine Abteilung dessen Name Ihrer Matrikelnummer entspricht.
    Fügen Sie dann dieser Abteilung Mitarbeiter hinzu. Die Anzahl der Mitarbeiter
    in der Abteilung soll der Quersumme der letzten zwei Ziffern Ihrer
    Matrikelnummer entsprechen.

Beispielsweise ergibt die Matrikelnummer: 00123456
eine Anzahl von 5 + 6 = 11.

Hinterlegen Sie neue Mitarbeiter falls notwendig.

   - Datei: `students_department.sql`
   - Platzhalter: nicht relevant
   - Rückgabeformat: nicht relevant

Damit die Tests bei Ihnen lokal erfolgreich durchlaufen, ändern Sie die Daten
in der Datei `.student.json` im root-Verzeichnis der Anwendung.


## 3. Aufgabe: Fertigungsrückmeldungen

In einem verteilten Fertigungsnetzwerk soll die Verwaltung von 
Rückmeldungen aus der Fertigung über eine Datenbank-Anwendung realisiert 
werden. Dabei sollen folgende Informationen geeignet verwaltet werden: 
Fertigungsrückmeldungsnummer, Datum und Uhrzeit der Fertigungsrückmeldung, 
Maschinennummer, Maschinenbezeichnung, Koordinaten (Breiten- und Längengrad) 
der Maschine bei transportablen Geräten, Werkzeugnummer und -bezeichnung des 
verwendeten Werkzeuges, und die Dauer des rückgemeldeten Fertigungsvorganges. 
Jede Maschine hat einen festgelegten Maschinenbetreuer (Personalnummer, Vor- 
und Nachname). Werkzeuge können an mehreren verschiedenen Maschinen eingesetzt 
werden und besitzen eine Lebensdauer.

Ein initialer Datensatz ist in der Datei `assignments/hw/testdata.csv` zu 
finden. Hier ein Auszug:

```
$rueckmeldeNummer,$datum,$uhrzeit,$werkzeugNummer,$maschinenNummer,$vorgangssdauer,$maschinenBezeichnung,$hauptstandort,$maschinenBetreuerSVNR,$maschinenBetreuerVorname,$maschinenBetreuerNachname,$werkzeugBezeichnung,$standzeit,$laengengrad,$breitengrad
1234410,2016-09-18,07:21:25,M-4432,M0201,62,"Bearbeitungszentrum 2",HS08,19,Monika,"Müller","Schrupp Fräser 40/120",15000,,
1234413,2016-09-18,07:21:25,,H0011,,"Mobiler HandlingRoboter ADD 22-1",HS08,21,Siegfried,Stinger,,,16.362252,48.200269
1234414,2016-09-18,07:21:25,,H0012,,"Mobiler HandlingRoboter ADD 22-2",HS02,21,Siegfried,Stinger,,,16.367658,48.199014
...
```

#### Datenbanktabellen erstellen
Fügen Sie der Datei `solutions/hw/migration_up.sql` die SQL-Befehle hinzu um 
Ihre Tabellen zu erstellen. Verwenden sie für die Speicherung der Mitarbeiter 
die Tabelle aus der ersten Hausübung.

Durch klicken auf die Schaltfläche "Tabellen erstellen" wird der SQL-Code in
dieser Datei ausgeführt. Möchten Sie die Tabellen entfernen, ohne die Anwendung 
neu zu starten, können Sie in der Datei `solutions/hw/migration_down.sql` 
die entsprechenden Befehle hinterlegen (`DROP TABLE`). Zugänglich ist dies über 
die Schaltfläche "Tabellen entfernen". 


#### Datenbank befüllen
Erstellen Sie eine Datenbanktransaktion um die Daten in Ihre Tabellen 
einzufügen. Schreiben sie ihr SQL in die Datei: 
`solutions/hw/data_intake.sql`. Vergessen Sie nicht auch eventuell neue 
Mitarbeiter in der Datenbank zu hinterlegen und bei einem `INSERT` anzugeben 
was bei einem Konflikt geschehen soll (`INSERT OR IGNORE INTO ...`).

Die Testdaten können dann mit der Schaltfläche 'Daten einlesen' in der 
Datenbank hinzugefügt werden. 


#### Abfragen
Die im Folgenden angegebenen Dateinamen beziehen sich auf den Pfad: 
`solutions/hw`.

Auf Basis der von Ihnen erstellten Tabellen schreiben Sie eine SQL-Abfrage...

  1. ... mit der alle Maschinen und ihre Betreuer ausgegeben werden können.
    * Datei: `machines_list_person_in_charge.sql`
    * Platzhalter: nicht relevant
    * Rückgabeformat: `(machineNumber, machineDescription, forename, surename)`

  2. ... mit der alle Maschinen aufgelistet werden können. Sortiert nach der 
  Maschinennummer.
    * Datei: `machines_all.sql`
    * Platzhalter: nicht relevant
    * Rückgabeformat: `(machineNumber, machineDescription)`

  3. ... mit der alle Werkzeuge die auf einer Maschine eingesetzt wurden 
  abgefragt werden kann. Achten Sie darauf, dass keine Ergebniszeilen doppelt 
  vorkommen.
    * Datei: `machines_tools_used.sql`
    * Platzhalter: `$maschineNumber`
    * Rückgabeformat: `(machineDescription, toolDescription)`

  4. ... mit der die Koordinaten der Einsatzorte einer Maschine abgefragt 
  werden können. Das Ergebnis soll nach dem Zeitpunkt der Ortänderungsmeldung 
  sortiert werden. Letzte Änderung zuerst.
    * Datei: `machines_sites.sql`
    * Platzhalter: `$maschineNumber`
    * Rückgabeformat: `(longitude, latitude)`

  5. ... mit der die Anzahl an Bewegungen einer Maschine ausgegeben werden 
  kann. Achten Sie darauf, dass auch Maschinen ohne Bewegung im Ergebnis 
  enthalten sind. Ergebnis absteigend sortiert nach der Anzahl an Bewegungen.
    * Datei: `machine_movements.sql`
    * Platzhalter: nicht relevant
    * Rückgabeformat: `(machineNumber, movements)`

  6. ... mit der die Gesamteinsatzzeit pro Werkzeug ausgegeben werden kann. 
  Ergebnis absteigend sortiert nach der Gesamteinsatzzeit.
    * Datei: `tools_operating_time.sql`
    * Platzhalter: nicht relevant
    * Rückgabeformat: `(toolNumber, operatingTime)`

  7. ... mit der die **drei** Werkzeuge mit der niedrigsten Restlebensdauer 
  ausgegeben werden können.
    * Datei: `tools_remaining_life.sql`
    * Platzhalter: nicht relevant
    * Rückgabeformat: `(toolNumber, remainingLife)`


#### Hinweis zur Abgabe

Sie können die abzugebende Datei mit dem Kommandozeilenbefehl
`npm run release hw` erstellen lassen. Eine Zip-Datei zum Hochladen im TUWEL
finden Sie dann im Ordner `releases`. Kontrollieren Sie den Inhalt der
Zip-Datei bevor Sie sie hochladen.


#### Hinweis zum Testen

Sie können die von Ihnen erstellten Abfragen mit dem Kommandozeilenbefehl
`npm run test hw` automatisch überprüfen. Dazu müssen Sie natürlich
die Anwendung installiert haben. Beschrieben in der [README](/).


#### Probleme, Unklarheiten

Kommen Sie zu den Tutorien, schreiben Sie Ihre Fragen ins TISS Forum.

Wir sind sehr bemüht Ihnen eine fehlerfreie Anwendung und Tests
bereitzustellen, leider können sich trotzdem Fehler einschleichen. Falls Sie
einen gefunden haben, bitte wir Sie um Kontaktaufnahme unter
paul.weissenbach@tuwien.ac.at.
