var db = require('../config/db.js')
var expect = require('expect.js')
var fs = require('fs')
var async = require('async')
var path = require('path')
var csv = require('fast-csv')

var solutionFolder = process.env.SOLUTIONS_FOLDER || 'solutions/'

// if this is needed in a third place, deduplicate it
function insertData (testdata, sql, callback) {
  // just a _quick and dirty_ replace.
  // it's one of the things you should never do in a production system
  // because it opens up the system for sql injections
  // right way would be to execute each insert with driver and use transaction
  // this would make the assignment more complicated to achieve
  async.each(testdata, function (testdata, cb) {
    var transaction = sql
    Object.keys(testdata).forEach(function (key) {
      transaction = transaction.split(key).join(testdata[key] ? "'" + testdata[key] + "'" : 'null')
    })

    db.exec(transaction, function (err) {
      if (err) return cb(err)
      cb()
    })
  },
  callback
  )
}

describe('Vorbereitung der Tests', function () {
  // in an actual test setup, this could be done in an before
  // hook (https://mochajs.org/#hooks)
  describe('Erstellen der Datenbanktabellen (hw/migration_up_org.sql)', function () {
    it('soll ohne Fehler durchlaufen', function (done) {
      var sql = fs.readFileSync(path.join(__dirname, '..', 'assignments/hw/migration_up_org.sql'), 'utf8')
      db.exec(sql, function (err) {
        expect(err).not.to.be.ok()
        done()
      })
    })
  })

  // this needs also to happen when the programm is executed
  // don't want to put it into the testdata.sql because it would be the
  // solution for 1.2.1
  describe('Mitarbeiter einfügen (hw/employee_add.sql)',
    function () {
      it('Testdaten ohne Fehler hinzufügen', function (done) {
        var employeesToInsert = require('../assignments/hw/testdata.json')
        async.each(employeesToInsert,
          function (employee, cb) {
            db.withSQLFromFile('hw/employee_add.sql')
              .run(employee, cb)
          },
          function (err) {
            expect(err).to.not.be.ok()
            done()
          }
        )
      })
    }
  )

  describe('Einfügen der restlichen Testdaten (hw/testdata.sql)', function () {
    it('soll ohne Fehler durchlaufen', function (done) {
      var sql = fs.readFileSync(path.join(__dirname, '..', 'assignments/hw/testdata.sql'), 'utf8')
      db.exec(sql, function (err) {
        expect(err).not.to.be.ok()
        done()
      })
    })
  })
})

describe('Hausübung', function () {
  describe('1. Aufgabe: Konzeptioneller Datenbankentwurf ', function () {
    it('Im Verzeichnis `solutions/hw` soll genau eine PDF-Datei vorhanden sein',
      function (done) {
        fs.readdir(path.join(solutionFolder, 'hw'), function (err, files) {
          expect(err).to.not.be.ok()
          var patt1 = /\.pdf$/i
          var pdfs = files.filter(
            function (filename) {
              return !!filename.match(patt1)
            })
          expect(pdfs).to.have.length(1)
          done()
        })
      }
    )
  })

  describe('2. Aufgabe: Mitarbeiterverwaltung', function () {
    describe('Abfrage 1: Mitarbeiter hinzufügen (hw/employee_add.sql)', function () {
      it('soll einen Mitarbeiter ohne Fehler hinzufügen ', function (done) {
        db.withSQLFromFile('hw/employee_add.sql')
          .run({
            $ssn: 500,
            $surname: 'Franz',
            $forename: 'Führich',
            $rate: 40
          },
          function (err) {
            expect(err).not.to.be.ok()
            done()
          }
          )
      })
      it('soll Fehlschlagen, falls es die Personalnummer schon gibt', function (done) {
        db.withSQLFromFile('hw/employee_add.sql')
          .run({
            $ssn: 500,
            $surname: 'Solte',
            $forename: 'Egalsen',
            $rate: 40
          },
          function (err) {
            expect(err).to.be.ok()
            done()
          }
          )
      })
      it('soll einen Mitarbeiter mit gleichem Namen aber unterschiedlicher Personalnummer ohne Fehler hinzufügen',
        function (done) {
          db.withSQLFromFile('hw/employee_add.sql')
            .run({
              $ssn: 501,
              $surname: 'Franz',
              $forename: 'Führich',
              $rate: 45
            },
            function (err) {
              expect(err).not.to.be.ok()
              done()
            }
            )
        }
      )
    })

    describe('Abfrage 2: Mitarbeitername und Stundensatz (hw/employees_all.sql)', function () {
      it('soll alle Mitarbeiter ohne Fehler zurückgeben', function (done) {
        db.withSQLFromFile('hw/employees_all.sql')
          .all(function (err, result) {
            expect(err).to.not.be.ok()
            expect(result).to.have.length(24)
            expect(result[0]).to.only.have.keys('ssn', 'forename', 'surname', 'rate')
            done()
          })
      })
    })

    describe('Abfrage 3: Erhöhung des Stundensatzes (hw/employee_increase_rate.sql)', function () {
      it('soll den Stundensatz eines Mitarbeiters um 10 erhöhen', function (done) {
        db.withSQLFromFile('hw/employee_increase_rate.sql')
          .run({ $ssn: 9, $rateIncrement: 10 },
            function (err) {
              expect(err).not.to.be.ok()
              db.get('SELECT Stundensatz AS rate FROM Mitarbeiter WHERE SVNR = $ssn;',
                { $ssn: 9 },
                function (err, result) {
                  expect(err).to.not.be.ok()
                  expect(result.rate).to.be(47)
                  done()
                })
            }
          )
      })
      it('soll den Stundensatz andere Mitarbeiter unberührt lassen', function (done) {
        db.get('SELECT Stundensatz AS rate FROM Mitarbeiter WHERE SVNR = $ssn;',
          { $ssn: 8 },
          function (err, result) {
            expect(err).to.not.be.ok()
            expect(result.rate).to.be(55)
            done()
          })
      })
    })

    describe('Abfrage 4: Mitarbeiter zur Abteitlung (hw/employee_add_to_department.sql)', function () {
      it('soll ohne Fehler für Mitarbeiter ohne Abteilung funktioneren',
        function (done) {
          db.withSQLFromFile('hw/employee_add_to_department.sql')
            .run({ $ssn: 20, $departmentShort: 'Kon' },
              function (err) {
                expect(err).to.not.be.ok()
                done()
              }
            )
        }
      )
      it('soll Fehlschlagen, falls Mitarbeiter nicht exisitiert', function (done) {
        db.withSQLFromFile('hw/employee_add_to_department.sql')
          .run({ $ssn: 999, $departmentShort: 'Kon' },
            function (err) {
              expect(err).to.be.ok()
              expect(err.code).to.eql('SQLITE_CONSTRAINT')
              done()
            }
          )
      })
      it('soll Fehlschlagen, falls die Abteilung nicht existiert', function (done) {
        db.withSQLFromFile('hw/employee_add_to_department.sql')
          .run({ $ssn: 21, $departmentShort: 'GibtsNicht' },
            function (err) {
              expect(err).to.be.ok()
              expect(err.code).to.eql('SQLITE_CONSTRAINT')
              done()
            }
          )
      })
      it('soll Fehlschlagen, falls der Mitarbeiter bereits einer anderen Abteilung zugehörig ist',
        function (done) {
          db.withSQLFromFile('hw/employee_add_to_department.sql')
            .run({ $ssn: 9, $departmentShort: 'Kon' }, // Employee #9 is in Pro
              function (err) {
                expect(err).to.be.ok()
                expect(err.code).to.be('SQLITE_CONSTRAINT')
                done()
              }
            )
        }
      )
    })

    describe('Abfrage 5: Mitarbeiter in Abteilung (hw/employees_in_department.sql)', function () {
      it('soll alle Mitarbeiter in einer Abeilung ohne Fehler auflisten',
        function (done) {
          db.withSQLFromFile('hw/employees_in_department.sql')
            .all({ $departmentShort: 'Kon' },
              function (err, result) {
                expect(err).to.not.be.ok()
                expect(result).to.have.length(4)
                expect(result[0]).to.only.have.keys('ssn')
                done()
              }
            )
        }
      )
    })

    describe('Abfrage 6: Manager einer Abteilung ändern (hw/department_set_manager.sql)', function () {
      it('soll den Leiter einer Abteilung setzen', function (done) {
        db.withSQLFromFile('hw/department_set_manager.sql')
          .run({ $departmentShort: 'Kon', $ssn: 13 },
            function (err) {
              expect(err).to.not.be.ok()
              done()
            }
          )
      })
      it('soll andere Abteilungen unberührt lassen', function (done) {
        // only testing one other department, not all of them
        db.get("SELECT Manager FROM Abteilungen WHERE Name='Pro'",
          function (err, result) {
            expect(err).to.not.be.ok()
            expect(result.Manager).to.eql(2)
            done()
          }
        )
      })
      it('soll Fehlschlagen, wenn Mitarbeiter nicht existiert', function (done) {
        db.withSQLFromFile('hw/department_set_manager.sql')
          .run({ $departmentShort: 'Kon', $ssn: 999 },
            function (err) {
              expect(err).to.be.ok()
              expect(err.code).to.be('SQLITE_CONSTRAINT')
              done()
            }
          )
      })
    })

    describe('Abfrage 7: Auflösen einer Abteilung ohne Mitarbeiter (hw/department_remove.sql)',
      function () {
        it('soll Abteilung ohne Fehler entfernen', function (done) {
          db.withSQLFromFile('hw/department_remove.sql')
            .run({ $departmentShort: 'Ent' }, // Entwicklugn hat keine Mitarbeiter.
              function (err) {
                expect(err).to.not.be.ok()
                done()
              }
            )
        })
        it('soll Fehlschlagen, wenn der Abteilung noch Mitarbeiter zugehörig sind', function (done) {
          db.withSQLFromFile('hw/department_remove.sql')
            .run({ $departmentShort: 'Kon' },
              function (err) {
                expect(err).to.be.ok()
                expect(err.code).to.be('SQLITE_CONSTRAINT')
                done()
              }
            )
        })
        it('soll andere Abteilungen unberührt lassen', function (done) {
          db.get('SELECT COUNT() AS countDepartments FROM Abteilungen',
            function (err, result) {
              expect(err).to.not.be.ok()
              expect(result.countDepartments).to.be(5)
              done()
            }
          )
        })
      }
    )

    describe('Abfrage 8: Mitarbeiter mit höchstem Stundensatz (hw/employee_max_rate.sql)', function () {
      it('soll den Mitarbeiter mit dem höchsten Stundensatz zurückgeben',
        function (done) {
          db.withSQLFromFile('hw/employee_max_rate.sql')
            .get(function (err, result) {
              expect(err).to.not.be.ok()
              expect(result).to.eql({ ssn: 8, rate: 55 })
              done()
            })
        }
      )
    })

    describe('Abfrage 9: Mitarbeiter in Projekt (hw/employees_in_project.sql)', function () {
      it('soll Mitarbeiter nicht mehrfach auflisten', function (done) {
        db.withSQLFromFile('hw/employees_in_project.sql')
          .all({ $projectName: 'RevolutionaryProduct' },
            function (err, result) {
              expect(err).to.not.be.ok()
              expect(result).to.have.length(4)
              expect(result[0]).to.only.have.keys('ssn')
              done()
            }
          )
      })
      it('soll auch für andere Projekte funktionieren', function (done) {
        db.withSQLFromFile('hw/employees_in_project.sql')
          .all({ $projectName: 'NewNewProduct' },
            function (err, result) {
              expect(err).to.not.be.ok()
              expect(result).to.have.length(1)
              expect(result[0]).to.only.have.keys('ssn')
              done()
            }
          )
      })
    })

    describe('Abfrage 10: Mitarbeiter in Projekt (hw/students_department.sql)', function () {
      var matrikelnummer = require(path.join(process.env.STUDENT_FILE || '..', '.student.json')).matrikelnummer

      console.log(matrikelnummer)

      it('vor ausführen der Aufgabe, soll keine Abteilung entsprechend der Matrikelnummer exisiteren',
        function (done) {
          db.get('SELECT COUNT() as studentTables FROM sqlite_master WHERE tbl_name = ? OR tbl_name = ?', [matrikelnummer, matrikelnummer.substr(1)],
            function (err, result) {
              expect(err).not.to.be.ok()
              expect(result.studentTables).to.be(0)
              done()
            })
        }
      )

      it('soll eine Abteilung mit Mitarbeiter entsprechend Ihrer Matrikelnummer erstellen', function (done) {
        db.withSQLFromFile('hw/students_department.sql')
          .exec(function (err) {
            expect(err).not.to.be.ok()

            db.get('SELECT COUNT() as employee_count FROM arbeitet_in WHERE Abteilung = ? OR Abteilung = ?', [matrikelnummer, matrikelnummer.substr(1)],
              function (err, result) {
                expect(err).not.to.be.ok()
                // quersumme der letzten zwei Ziffern der Matrikelnummer
                var expectedEmployeeCount = matrikelnummer
                  .slice(-2)
                  .split('')
                  .reduce(function (accumulator, currentValue) {
                    return accumulator + parseInt(currentValue, 10)
                  }, 0)

                expect(result.employee_count).to.eql(expectedEmployeeCount)
                done()
              })
          })
      })
    })
  })

  describe('3. Aufgabe: Fertigungsrückmeldungen', function () {
    it('Datenbankmigration soll ohne Fehler durchlaufen (`hw/migration_up.sql`)', function (done) {
      db.withSQLFromFile('hw/migration_up.sql')
        .exec(function (err) {
          expect(err).not.to.be.ok()
          done()
        })
    })
    it('Einfügen der Datensätze soll ohne Fehler durchlaufen (`hw/data_intake.sql`)', function (done) {
      var testdataProduction = []
      csv.parseFile(path.join(__dirname, '..', 'assignments/hw/testdata.csv'), { headers: true })
        .on('data', function (data) {
          testdataProduction.push(data)
        })
        .on('end', function () {
          var sqlTemplate = fs.readFileSync(path.join(solutionFolder, 'hw/data_intake.sql'), 'utf8')
          insertData(testdataProduction, sqlTemplate, function (err) {
            expect(err).to.not.be.ok()
            done()
          })
        })
    })

    describe('Aufgabe: Abfragen', function () {
      describe('Maschinen mit Betreuernamen ausgeben (`hw/machines_list_person_in_charge.sql`)', function () {
        it('soll ohne Fehler durchlaufen, 6 Zeilen zurückgeben und das richtige Rückgabeformat aufweisen', function (done) {
          db.withSQLFromFile('hw/machines_list_person_in_charge.sql')
            .all(function (err, results) {
              expect(err).to.not.be.ok()
              expect(results).to.have.length(6)
              expect(results[0]).to.only.have.keys('machineNumber', 'machineDescription', 'forename', 'surname')
              done()
            })
        })
      })

      describe('Alle Maschinen zurückgeben (`hw/machines_all.sql`)', function () {
        it('soll ohne Fehler durchlaufen und erwartetes Ergebnis zurückliefern', function (done) {
          db.withSQLFromFile('hw/machines_all.sql')
            .all(function (err, results) {
              expect(err).to.not.be.ok()
              expect(results).to.have.length(6)
              expect(results).to.eql([
                { machineNumber: 'B0012', machineDescription: 'Blechbearbeitungszentrum TR 220' },
                { machineNumber: 'H0011', machineDescription: 'Mobiler HandlingRoboter ADD 22-1' },
                { machineNumber: 'H0012', machineDescription: 'Mobiler HandlingRoboter ADD 22-2' },
                { machineNumber: 'H0021', machineDescription: 'Mobiler HandlingRoboter ADD 22-3' },
                { machineNumber: 'M0200', machineDescription: 'Bearbeitungszentrum 1' },
                { machineNumber: 'M0201', machineDescription: 'Bearbeitungszentrum 2' }
              ])
              done()
            })
        })
      })

      describe('Werkzeuge die auf einer Maschine verwendet wurden (`hw/machines_tools_used.sql`)', function () {
        it('soll ohne Fehler durchlaufen und erwartetes Ergebnis zurückliefern', function (done) {
          db.withSQLFromFile('hw/machines_tools_used.sql')
            .all({
              $machineNumber: 'M0200'
            },
            function (err, results) {
              expect(err).to.not.be.ok()
              expect(results).to.have.length(5)
              done()
            })
        })
      })

      describe('Einsatzorte einer Maschine (`hw/machines_sites.sql`)', function () {
        it('soll ohne Fehler durchlaufen und erwartetes Ergebnis für Maschine "H0011" liefern', function (done) {
          db.withSQLFromFile('hw/machines_sites.sql')
            .all({
              $machineNumber: 'H0011'
            }, function (err, results) {
              expect(err).to.not.be.ok()
              expect(results).to.have.length(3)
              expect(results).to.eql([
                { longitude: 16.366918, latitude: 48.199214 },
                { longitude: 16.368173, latitude: 48.199028 },
                { longitude: 16.362252, latitude: 48.200269 }
              ])
              done()
            })
        })
      })

      describe('Maschinenbewegungen (`hw/machine_movements.sql`)', function () {
        it('soll ohne Fehler durchlaufen und 6 Ergebniszeilen liefern die nach der Anzahl an Bewegungen sortiert sind', function (done) {
          db.withSQLFromFile('hw/machine_movements.sql')
            .all(function (err, results) {
              expect(err).to.not.be.ok()
              expect(results).to.have.length(6)
              // the order of the last three rows is undefined,
              // so we only check the first three
              expect(results.slice(0, 3)).to.eql([
                { machineNumber: 'H0012', movements: 4 },
                { machineNumber: 'H0011', movements: 3 },
                { machineNumber: 'H0021', movements: 2 }
              ])
              done()
            })
        })
      })

      describe('Werkzeug Gesamteinsatzzeit (`hw/tools_operating_time.sql`)', function () {
        it('soll ohne Fehler durchlaufen und das erwartete Ergebnis liefern', function (done) {
          db.withSQLFromFile('hw/tools_operating_time.sql')
            .all(function (err, results) {
              expect(err).to.not.be.ok()
              expect(results).to.eql([
                { toolNumber: 'M-4484', operatingTime: 1139 },
                { toolNumber: 'M-4432', operatingTime: 744 },
                { toolNumber: 'M-3313', operatingTime: 351 },
                { toolNumber: 'M-3314', operatingTime: 165 },
                { toolNumber: 'S-1252', operatingTime: 141 },
                { toolNumber: 'S-1251', operatingTime: 93 },
                { toolNumber: 'S-1202', operatingTime: 86 },
                { toolNumber: 'S-1201', operatingTime: 54 },
                { toolNumber: 'M-4442', operatingTime: 53 },
                { toolNumber: 'S-1250', operatingTime: 12 }
              ])
              done()
            })
        })
      })

      describe('Restlebendauer der Werkzeuge (`hw/tools_remaining_life.sql`)', function () {
        it('soll ohne Fehler durchlaufen, 3 Ergebniszeilen liefern, die mit dem erwarteten Ergebnis übereinstimmen', function (done) {
          db.withSQLFromFile('hw/tools_remaining_life.sql')
            .all(function (err, results) {
              expect(err).to.not.be.ok()
              expect(results).to.have.length(3)
              expect(results).to.eql([
                { toolNumber: 'M-4442', remainingLife: 9947 },
                { toolNumber: 'M-4432', remainingLife: 14256 },
                { toolNumber: 'M-4484', remainingLife: 78861 }
              ])
              done()
            })
        })
      })
    })
  })
})
