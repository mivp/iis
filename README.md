[tuwel_course_address]: https://tuwel.tuwien.ac.at/course/view.php?idnumber=330286-2019W
[repository]: https://bitbucket.org/mivp/iis
[tiss_course_address]: https://tiss.tuwien.ac.at/course/courseDetails.xhtml?dswid=2133&dsrid=759&courseNr=330286&semester=2019W

# Übung: Industrielle Informationssysteme (2019W)
Diese Webanwendung ist __Teil__ der Übung Industrielle Informationssysteme (330.286)
in welcher Grundlagen relationaler Datenbanken, Datenmodellierung und SQL vermittelt
werden. Diese Anwendung dient zur Illustration und als Kontext für Hausübungen 
und ist keinesfalls für einen produktiven Einsatz geeignet.

Informationen zur Durchführung der Lehrveranstaltung finden sich in
[TUWEL][tuwel_course_address] und [TISS][tiss_course_address].

#### Zu den Aufgaben
  * [Hausübung](/assignments/hw/hw.md)

#### Beispiele aus dem Vortrag
  * [Organisation](/assignments/lectures/lecture1.md)
  * [Tabellen erstellen, befüllen und abfragen](/assignments/lectures/lecture2.md)


## Installation
Folgende Schritte sind notwendig um die Aufgaben zu erfüllen. Die einzelnen Punkte 
sind im Weiteren detailiert erklärt.

  * Installation von [node.js](http://nodejs.org) (LTS)
  * Download des [Source Codes der Anwendung][repository]
  * Mittels Terminal/Kommandozeile in den Anwendungsordner navigieren
  * Installieren der von der Anwendung benötigtet Module (`npm install`)
  * Starten der Anwendung (`npm start`)
  * Abgeben der erstellten Lösungen (`npm run release` und die dadurch erstellte Datei im TUWEL hochladen)

#### Installation node.js 
Um die Anwendung auszuführen ist die Runtime node.js notwendig. Diese Runtime können 
Sie auf [nodejs.org](http://nodejs.org) herunterladen. Wählen sie die LTS (long 
time support) Option und folgen Sie dort den Anweisungen.

#### Installation Anwendung und benötige Module
Laden Sie die Anwendung vom [Sourcecode Repository][repository]. Sie können 
entweder eine Zip-Datei (unter dem Menupunkt "Downloads") herunterladen, oder 
die Anwendung mittels `git` installieren. Mehr Informationen zu `git` finden Sie 
auf [git-scm.com](https://git-scm.com/).

Entpacken sie gegebenenfalls die heruntergeladene Datei und öffnen sie den Ordner 
[im Terminal/in der Kommandozeile](https://de.wikipedia.org/wiki/Kommandozeile).

```bash
cd iis
```

Mit `cd` (change directory) können Sie das Verzeichnis wechseln. Befindet sich 
die entpackte Hausübung beispielsweise im Verzeichnis `\Users\mivp\Desktop\iis` 
können Sie mittels `cd \Users\mivp\Desktop\iis` dorthin wechseln.

Mittels `dir` können Sie sich in der Windowskommandozeilenanwendung den Inhalt 
eines Verzeichnisses auflisten lassen. Gleiches erreichen Sie mit `ls` auf 
Unix basierenden Systemen (macOS, Linux oder in der Windows PowerShell).


Die Anwendung benötigt Module. Diese können mit folgendem Befehl in der 
Kommandozeile installiert werden. Npm (node package manager) wird mit nodejs
mitinstalliert.

```bash
npm install
```

Die Datenbank (in unserem Fall [SQLite3](https://www.sqlite.org/)) wird als
eines dieser Module mitinstalliert. Wir verwenden SQLite um Ihnen das Leben 
etwas zu erleichtern. SQLite ist nicht vergleichbar mit client/server 
Datenbanken, wie MySQL, Oracle, PostgreSQL, etc. die üblicherweise in 
industriellen Informationssystemen als geteilte Datenablage dienen. Mehr 
Informationen zu geeigneten Einsatzmöglichen finden sie 
[hier](https://www.sqlite.org/whentouse.html).

![Installation](/public/images/install-app.gif)


Die Installation sollte auf den meisten Systemen fehlerfrei durchlaufen.
Sollten Sie eine Plattform verwenden, für die das SQLite Modul kein 
kompiliertes Binary mitliefert melden Sie sich bitte unter 
paul.weissenbach@tuwien.ac.at.


## Starten und Stoppen der Anwendung
Wurde die Anwendung fehlerfrei installiert, kann sie mittels folgendem Befehl 
im Terminal/in der Kommandozeile gestartet werden:

```bash
npm start
```

Wenn nichts Anderes konfiguriert wurde, können Sie im Browser unter
[http://localhost:3000](http://localhost:3000) auf die Anwendung zugreifen.

Beendet werden kann die Anwendung mit der Tastenkombination: 
<kbd><kbd>Strg</kbd> + <kbd>C</kbd></kbd> in der Kommandozeile.

![Start Stop](/public/images/run-app.gif)

## Testen der SQL-Abfragen
Beim Testen werden Funktionen der Anwendung automatisch ausgeführt um
sicherzustellen, dass diese die erwarteten Rückgaben liefern. Möchten Sie
Ihre Hausübung überprüfen, führen sie folgenden Befehl aus:

```bash
npm run test
```

Eine fehlerloser durchlauf der Tests sieht aus wie im folgendem Bild 
dargestellt.

![Testen der ersten Hausübung](/public/images/run-script-test-hw1.gif)



Wir führen denselben Code aus um Ihre Abgabe auf Funktionstüchtigkeit zu
überprüfen. Solche Tests können im Allgemeinen auch nur Fehler 
aufzeigen, nicht die Abwesenheit von Fehlern garantieren.


## Abgeben

```bash
npm run release
```

Dieser Befehl erstellt eine Zip-Datei (im Ordner `releases`), welche Sie im __TUWEL 
hochladen__ sollen. 

![Release Homework](/public/images/run-release.gif)


Kontrollieren Sie den Inhalt bevor sie die Datei hochladen, es sollten alle Dateien 
aus dem Ordner `solutions/hw` enthalten sein. Der  


## Abgabedokument

Unter der Bedingung, dass sich ihre Lösung (die Zip-Datei) im `releases` Ordner 
befindet und als `hw.zip` benannt wurde können Sie mit folgendem Befehl das 
Abgabedokument für die erste Hausübung selbst erstellen. Das erstellte Dokument sollten 
sie dann im Ordner `releases` finden.

```bash
npm run report
```

![Start Stop](/public/images/run-report.gif)


## Warnung
Diese Anwendung ist __nicht__ für einen produktiven Einsatz geeignet. 
Beispielsweise sind SQL Abfragen in einzelne Dateien abgelegt, die bei jeder 
Anfrage erneut vom Dateisystem gelesen werden.
Grundsätzlich wurden keine Überlegungen bezüglich Performance oder Security
angestellt. Die Tests sind für die Überprüfung der Aufgaben ausgelegt.


## Kontakt
Fragen und Anregungen bitte an paul.weissenbach@tuwien.ac.at. Studenten können 
auch in den Tutorien Fragen stellen. Die Tutorienterminen sind im 
[TISS][tiss_course_address] eingetragen.


# Lizenz
MIT
